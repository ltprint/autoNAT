package com.brsya.autonat;

import android.app.Application;
import android.content.SharedPreferences;

/**
 * Created by Brsya
 * CreateDate: 2023/2/11  19:37
 * Description:
 */
public class MyApplication extends Application {
    public static Application application;

    public static SharedPreferences sharedPreferences;

    @Override
    public void onCreate() {
        super.onCreate();
        application = this;

        //获取SharedPreferences对象
        sharedPreferences = getSharedPreferences("user_config", MODE_PRIVATE);
    }

    public static void putString(String key, String value) {
        if (sharedPreferences == null) {
            return;
        }
        //获取Editor对象的引用
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        // 提交数据
        editor.apply();
    }

    public static String getString(String key) {
        if (sharedPreferences == null) {
            return null;
        }
        return sharedPreferences.getString(key, null);
    }

    public static void putBoolean(String key, boolean value) {
        if (sharedPreferences == null) {
            return;
        }
        //获取Editor对象的引用
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(key, value);
        // 提交数据
        editor.apply();
    }

    public static boolean getBoolean(String key) {
        if (sharedPreferences == null) {
            return false;
        }
        return sharedPreferences.getBoolean(key, false);
    }


}
