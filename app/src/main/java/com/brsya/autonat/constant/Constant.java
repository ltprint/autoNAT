package com.brsya.autonat.constant;

/**
 * Created by Brsya
 * CreateDate: 2023/2/11  23:25
 * Description:
 * 常量
 */
public class Constant {
    /**
     * 是否打开连接蓝牙开启热点开关
     */
    public static final String KEY_IS_OPEN_AUTO_NAT = "key_is_open_auto_nat";

    /**
     * 是否打开显示信号开关
     */
    public static final String KEY_IS_OPEN_SIGNAL = "key_is_open_signal";

    /**
     * 选择蓝牙的名称
     */
    public static final String KEY_BLUETOOTH_NAME = "key_bluetooth_name";

    /**
     * 选择蓝牙的物理地址
     */
    public static final String KEY_BLUETOOTH_ADDRESS = "key_bluetooth_address";


}
