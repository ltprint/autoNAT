package com.brsya.autonat.bean;

/**
 * Created by Brsya
 * CreateDate: 2023/2/11  21:41
 * Description:
 * 蓝牙信息
 */
public class BluetoothBean {

    private int type;
    private String typeName;
    private String name;
    private String address;

    private boolean isSelect;

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public boolean isSelect() {
        return isSelect;
    }

    public void setSelect(boolean select) {
        isSelect = select;
    }
}
