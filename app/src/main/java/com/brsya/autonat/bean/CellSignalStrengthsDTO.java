package com.brsya.autonat.bean;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * Created by Brsya
 * CreateDate: 2023/2/24  15:56
 * Description:
 */
public class CellSignalStrengthsDTO {
    @JSONField(name = "asuLevel")
    private int asuLevel;
    @JSONField(name = "cqi")
    private int cqi;
    @JSONField(name = "cqiTableIndex")
    private int cqiTableIndex;
    @JSONField(name = "dbm")
    private int dbm;
    @JSONField(name = "level")
    private int level;
    @JSONField(name = "miuiLevel")
    private int miuiLevel;
    @JSONField(name = "rsrp")
    private int rsrp;
    @JSONField(name = "rsrq")
    private int rsrq;
    @JSONField(name = "rssi")
    private int rssi;
    @JSONField(name = "rssnr")
    private int rssnr;
    @JSONField(name = "timingAdvance")
    private int timingAdvance;

    public int getAsuLevel() {
        return asuLevel;
    }

    public void setAsuLevel(int asuLevel) {
        this.asuLevel = asuLevel;
    }

    public int getCqi() {
        return cqi;
    }

    public void setCqi(int cqi) {
        this.cqi = cqi;
    }

    public int getCqiTableIndex() {
        return cqiTableIndex;
    }

    public void setCqiTableIndex(int cqiTableIndex) {
        this.cqiTableIndex = cqiTableIndex;
    }

    public int getDbm() {
        return dbm;
    }

    public void setDbm(int dbm) {
        this.dbm = dbm;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getMiuiLevel() {
        return miuiLevel;
    }

    public void setMiuiLevel(int miuiLevel) {
        this.miuiLevel = miuiLevel;
    }

    public int getRsrp() {
        return rsrp;
    }

    public void setRsrp(int rsrp) {
        this.rsrp = rsrp;
    }

    public int getRsrq() {
        return rsrq;
    }

    public void setRsrq(int rsrq) {
        this.rsrq = rsrq;
    }

    public int getRssi() {
        return rssi;
    }

    public void setRssi(int rssi) {
        this.rssi = rssi;
    }

    public int getRssnr() {
        return rssnr;
    }

    public void setRssnr(int rssnr) {
        this.rssnr = rssnr;
    }

    public int getTimingAdvance() {
        return timingAdvance;
    }

    public void setTimingAdvance(int timingAdvance) {
        this.timingAdvance = timingAdvance;
    }
}
