package com.brsya.autonat.bean;

import com.alibaba.fastjson.annotation.JSONField;

import java.util.List;

/**
 * Created by Brsya
 * CreateDate: 2023/2/24  15:55
 * Description:
 * 系统信号
 */
public class SignalBean {

    @JSONField(name = "asuLevel")
    private int asuLevel;
    @JSONField(name = "cdmaAsuLevel")
    private int cdmaAsuLevel;
    @JSONField(name = "cdmaDbm")
    private int cdmaDbm;
    @JSONField(name = "cdmaEcio")
    private int cdmaEcio;
    @JSONField(name = "cdmaLevel")
    private int cdmaLevel;
    @JSONField(name = "cellSignalStrengths")
    private List<CellSignalStrengthsDTO> cellSignalStrengths;
    @JSONField(name = "dbm")
    private int dbm;
    @JSONField(name = "evdoAsuLevel")
    private int evdoAsuLevel;
    @JSONField(name = "evdoDbm")
    private int evdoDbm;
    @JSONField(name = "evdoEcio")
    private int evdoEcio;
    @JSONField(name = "evdoLevel")
    private int evdoLevel;
    @JSONField(name = "evdoSnr")
    private int evdoSnr;
    @JSONField(name = "gsm")
    private boolean gsm;
    @JSONField(name = "gsmAsuLevel")
    private int gsmAsuLevel;
    @JSONField(name = "gsmBitErrorRate")
    private int gsmBitErrorRate;
    @JSONField(name = "gsmDbm")
    private int gsmDbm;
    @JSONField(name = "gsmLevel")
    private int gsmLevel;
    @JSONField(name = "gsmSignalStrength")
    private int gsmSignalStrength;
    @JSONField(name = "level")
    private int level;
    @JSONField(name = "lteAsuLevel")
    private int lteAsuLevel;
    @JSONField(name = "lteCqi")
    private int lteCqi;
    @JSONField(name = "lteDbm")
    private int lteDbm;
    @JSONField(name = "lteLevel")
    private int lteLevel;
    @JSONField(name = "lteRsrp")
    private int lteRsrp;
    @JSONField(name = "lteRsrq")
    private int lteRsrq;
    @JSONField(name = "lteRssnr")
    private int lteRssnr;
    @JSONField(name = "lteSignalStrength")
    private int lteSignalStrength;
    @JSONField(name = "miuiLevel")
    private int miuiLevel;
    @JSONField(name = "tdScdmaAsuLevel")
    private int tdScdmaAsuLevel;
    @JSONField(name = "tdScdmaDbm")
    private int tdScdmaDbm;
    @JSONField(name = "tdScdmaLevel")
    private int tdScdmaLevel;
    @JSONField(name = "timestampMillis")
    private int timestampMillis;

    public int getAsuLevel() {
        return asuLevel;
    }

    public void setAsuLevel(int asuLevel) {
        this.asuLevel = asuLevel;
    }

    public int getCdmaAsuLevel() {
        return cdmaAsuLevel;
    }

    public void setCdmaAsuLevel(int cdmaAsuLevel) {
        this.cdmaAsuLevel = cdmaAsuLevel;
    }

    public int getCdmaDbm() {
        return cdmaDbm;
    }

    public void setCdmaDbm(int cdmaDbm) {
        this.cdmaDbm = cdmaDbm;
    }

    public int getCdmaEcio() {
        return cdmaEcio;
    }

    public void setCdmaEcio(int cdmaEcio) {
        this.cdmaEcio = cdmaEcio;
    }

    public int getCdmaLevel() {
        return cdmaLevel;
    }

    public void setCdmaLevel(int cdmaLevel) {
        this.cdmaLevel = cdmaLevel;
    }

    public List<CellSignalStrengthsDTO> getCellSignalStrengths() {
        return cellSignalStrengths;
    }

    public void setCellSignalStrengths(List<CellSignalStrengthsDTO> cellSignalStrengths) {
        this.cellSignalStrengths = cellSignalStrengths;
    }

    public int getDbm() {
        return dbm;
    }

    public void setDbm(int dbm) {
        this.dbm = dbm;
    }

    public int getEvdoAsuLevel() {
        return evdoAsuLevel;
    }

    public void setEvdoAsuLevel(int evdoAsuLevel) {
        this.evdoAsuLevel = evdoAsuLevel;
    }

    public int getEvdoDbm() {
        return evdoDbm;
    }

    public void setEvdoDbm(int evdoDbm) {
        this.evdoDbm = evdoDbm;
    }

    public int getEvdoEcio() {
        return evdoEcio;
    }

    public void setEvdoEcio(int evdoEcio) {
        this.evdoEcio = evdoEcio;
    }

    public int getEvdoLevel() {
        return evdoLevel;
    }

    public void setEvdoLevel(int evdoLevel) {
        this.evdoLevel = evdoLevel;
    }

    public int getEvdoSnr() {
        return evdoSnr;
    }

    public void setEvdoSnr(int evdoSnr) {
        this.evdoSnr = evdoSnr;
    }

    public boolean isGsm() {
        return gsm;
    }

    public void setGsm(boolean gsm) {
        this.gsm = gsm;
    }

    public int getGsmAsuLevel() {
        return gsmAsuLevel;
    }

    public void setGsmAsuLevel(int gsmAsuLevel) {
        this.gsmAsuLevel = gsmAsuLevel;
    }

    public int getGsmBitErrorRate() {
        return gsmBitErrorRate;
    }

    public void setGsmBitErrorRate(int gsmBitErrorRate) {
        this.gsmBitErrorRate = gsmBitErrorRate;
    }

    public int getGsmDbm() {
        return gsmDbm;
    }

    public void setGsmDbm(int gsmDbm) {
        this.gsmDbm = gsmDbm;
    }

    public int getGsmLevel() {
        return gsmLevel;
    }

    public void setGsmLevel(int gsmLevel) {
        this.gsmLevel = gsmLevel;
    }

    public int getGsmSignalStrength() {
        return gsmSignalStrength;
    }

    public void setGsmSignalStrength(int gsmSignalStrength) {
        this.gsmSignalStrength = gsmSignalStrength;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getLteAsuLevel() {
        return lteAsuLevel;
    }

    public void setLteAsuLevel(int lteAsuLevel) {
        this.lteAsuLevel = lteAsuLevel;
    }

    public int getLteCqi() {
        return lteCqi;
    }

    public void setLteCqi(int lteCqi) {
        this.lteCqi = lteCqi;
    }

    public int getLteDbm() {
        return lteDbm;
    }

    public void setLteDbm(int lteDbm) {
        this.lteDbm = lteDbm;
    }

    public int getLteLevel() {
        return lteLevel;
    }

    public void setLteLevel(int lteLevel) {
        this.lteLevel = lteLevel;
    }

    public int getLteRsrp() {
        return lteRsrp;
    }

    public void setLteRsrp(int lteRsrp) {
        this.lteRsrp = lteRsrp;
    }

    public int getLteRsrq() {
        return lteRsrq;
    }

    public void setLteRsrq(int lteRsrq) {
        this.lteRsrq = lteRsrq;
    }

    public int getLteRssnr() {
        return lteRssnr;
    }

    public void setLteRssnr(int lteRssnr) {
        this.lteRssnr = lteRssnr;
    }

    public int getLteSignalStrength() {
        return lteSignalStrength;
    }

    public void setLteSignalStrength(int lteSignalStrength) {
        this.lteSignalStrength = lteSignalStrength;
    }

    public int getMiuiLevel() {
        return miuiLevel;
    }

    public void setMiuiLevel(int miuiLevel) {
        this.miuiLevel = miuiLevel;
    }

    public int getTdScdmaAsuLevel() {
        return tdScdmaAsuLevel;
    }

    public void setTdScdmaAsuLevel(int tdScdmaAsuLevel) {
        this.tdScdmaAsuLevel = tdScdmaAsuLevel;
    }

    public int getTdScdmaDbm() {
        return tdScdmaDbm;
    }

    public void setTdScdmaDbm(int tdScdmaDbm) {
        this.tdScdmaDbm = tdScdmaDbm;
    }

    public int getTdScdmaLevel() {
        return tdScdmaLevel;
    }

    public void setTdScdmaLevel(int tdScdmaLevel) {
        this.tdScdmaLevel = tdScdmaLevel;
    }

    public int getTimestampMillis() {
        return timestampMillis;
    }

    public void setTimestampMillis(int timestampMillis) {
        this.timestampMillis = timestampMillis;
    }
}
