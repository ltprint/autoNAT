package com.brsya.autonat.receive;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.brsya.autonat.service.MyService;

/**
 * Created by Brsya
 * CreateDate: 2023/2/11  20:59
 * Description: 开机启动监听
 */
public class AutoStartBroadReceiver  extends BroadcastReceiver {
    private static final String ACTION = "android.intent.action.BOOT_COMPLETED";

    @Override
    public void onReceive(Context context, Intent intent) {
        //开机启动
        if (ACTION.equals(intent.getAction())) {
            //第一种方式：根据包名
            //            PackageManager packageManager = context.getPackageManager();
            //            Intent mainIntent = packageManager.getLaunchIntentForPackage("com.harry.martin");
            //            mainIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            //            context.startActivity(mainIntent);
            //            context.startService(mainIntent);


            //第二种方式：指定class类，跳转到相应的Acitivity
            Intent mainIntent = new Intent(context, MyService.class);
            /**
             * Intent.FLAG_ACTIVITY_NEW_TASK
             * Intent.FLAG_ACTIVITY_CLEAR_TOP
             */
            mainIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//            context.startActivity(mainIntent);
                        context.startService(mainIntent);
        }
    }
}
