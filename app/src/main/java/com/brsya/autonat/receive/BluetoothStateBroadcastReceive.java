package com.brsya.autonat.receive;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.brsya.autonat.MyApplication;
import com.brsya.autonat.constant.Constant;
import com.brsya.autonat.util.LogUtil;
import com.brsya.autonat.util.WifiApUtils;

/**
 * Created by Brsya
 * CreateDate: 2023/2/11  20:59
 * Description: 蓝牙监听
 */
public class BluetoothStateBroadcastReceive extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (action == null) {
            return;
        }
        BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
        switch (action) {
            case BluetoothDevice.ACTION_ACL_CONNECTED:
//                Toast.makeText(context, "蓝牙设备:" + device.getName() + "已链接", Toast.LENGTH_SHORT).show();

                LogUtil.e("蓝牙已连接,name:" + device.getName() +
                        ",address:" + device.getAddress() +
                        ",getType:" + device.getType() +
                        ",getUuids:" + device.getUuids() +
                        ",getBondState:" + device.getBondState()
                );


                if (MyApplication.getBoolean(Constant.KEY_IS_OPEN_AUTO_NAT)) {
                    String address = MyApplication.getString(Constant.KEY_BLUETOOTH_ADDRESS);
                    if (device.getAddress().equals(address)) {
                        LogUtil.e("打开热点");
                        WifiApUtils.openWifiAP();
                    }else{
                        LogUtil.e("地址不匹配，未打开热点");
                    }
                }else{
                    LogUtil.e("总开关未打开，未打开热点");
                }


                break;
            case BluetoothDevice.ACTION_ACL_DISCONNECTED:
//                Toast.makeText(context, "蓝牙设备:" + device.getName() + "已断开", Toast.LENGTH_SHORT).show();

                LogUtil.e("蓝牙已断开,name:" + device.getName() +
                        ",address:" + device.getAddress() +
                        ",getType:" + device.getType() +
                        ",getBondState:" + device.getBondState()
                );
                break;
            case BluetoothAdapter.ACTION_STATE_CHANGED:
                int blueState = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, 0);
                switch (blueState) {
                    case BluetoothAdapter.STATE_OFF:
//                        Toast.makeText(context, "蓝牙已关闭", Toast.LENGTH_SHORT).show();
                        break;
                    case BluetoothAdapter.STATE_ON:
//                        Toast.makeText(context, "蓝牙已开启", Toast.LENGTH_SHORT).show();
                        break;
                }
                break;
        }
    }
}