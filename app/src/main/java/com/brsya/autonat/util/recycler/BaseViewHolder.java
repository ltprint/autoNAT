package com.brsya.autonat.util.recycler;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

/**
 * 扩充ViewHolder功能
 */
public class BaseViewHolder extends RecyclerView.ViewHolder {

    public BaseViewHolder(@NonNull View itemView) {
        super(itemView);
    }

    public View getItemView(){
        return itemView;
    }

    public <T extends View> T findViewById(int id){
        return itemView.findViewById(id);
    }
}
