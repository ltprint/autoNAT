package com.brsya.autonat.util.recycler;

/**
 * Created by Brsya
 * CreateDate: 2022/7/6  9:11
 * Description:
 * 多布局itemBean
 */
public class BaseItemBean {
    private int itemType;

    public int getItemType() {
        return itemType;
    }

    public void setItemType(int itemType) {
        this.itemType = itemType;
    }
}
