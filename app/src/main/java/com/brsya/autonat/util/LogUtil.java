package com.brsya.autonat.util;

import android.util.Log;

/**
 * Created by Brsya
 * CreateDate: 2023/2/11  18:38
 * Description:
 * 日志工具
 */
public class LogUtil {

    private static final String TAG = "brsya____";

    public static void e(String msg){
        Log.e(TAG, "e: " + msg);
    }
}
