package com.brsya.autonat.util.recycler;

import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import java.util.ArrayList;
import java.util.List;

/**
 * 可设置HeadView的Adapter
 *
 * @param <T> 数据类型
 */
public abstract class BaseAdapter<T> extends RecyclerView.Adapter<BaseViewHolder> {
    public static final int TYPE_NORMAL = -10000;
    public static final int TYPE_HEADER = -10001;
    public static final int TYPE_BOTTOM = -10002;
    private List<T> dataList = new ArrayList<>();

    private View headerView;
    private View BottomView;

    private OnItemClickListener<T> itemListener;
    private OnItemViewClickListener<T> itemViewClickListener;

    @Override
    public int getItemViewType(int position) {
        // 有headView 第一个为headView
        if (headerView != null && position == 0) return TYPE_HEADER;
        // 没有headView 超出数据为bottomView
        if (headerView == null && position > dataList.size() - 1) return TYPE_BOTTOM;
        // 有headView 超出数据加headView为bottomView
        if (headerView != null && position > dataList.size()) return TYPE_BOTTOM;

        if (headerView != null) {
            return getItemLayoutType(position - 1);
        }

        // 普通情况
        return getItemLayoutType(position);
    }

    /**
     * 获取relPosition对应的Layout类型
     */
    public int getItemLayoutType(int relPosition) {
        return TYPE_NORMAL;
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // 如果是headView，新建headView的ViewHolder
        if (headerView != null && viewType == TYPE_HEADER) return new BaseViewHolder(headerView);
        // 如果是headView，新建headView的ViewHolder
        if (BottomView != null && viewType == TYPE_BOTTOM) return new BaseViewHolder(BottomView);
        // 创建itemView的viewHolder
        return onCreate(parent, viewType);
    }

    @Override
    public void onBindViewHolder( BaseViewHolder viewHolder, int position) {
        // 如果是headView，不绑定数据
        if (getItemViewType(position) == TYPE_HEADER) return;
        // 如果是headView，不绑定数据
        if (getItemViewType(position) == TYPE_BOTTOM) return;
        // 获取ViewHolder在数据列表中的位置
        final int pos = getRealPosition(viewHolder);
        // 获取数据
        final T data = this.dataList.get(pos);
        // 重写绑定事件
        onBind(viewHolder, pos, data);
        // 设置item的点击事件
        if (itemListener != null) {
            viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    itemListener.onItemClick(pos, data);
                }
            });
        }
        if (itemViewClickListener != null) {
            viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    itemViewClickListener.onItemClick(pos, data, v);
                }
            });
        }
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        // headView修改layoutManager为占整行
        RecyclerView.LayoutManager manager = recyclerView.getLayoutManager();
        if (manager instanceof GridLayoutManager) {
            final GridLayoutManager gridManager = ((GridLayoutManager) manager);
            gridManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                @Override
                public int getSpanSize(int position) {
                    int spanSizeBtPosition = getSpanSizeBtPosition(position);
                    if (spanSizeBtPosition == -1) {
                        return gridManager.getSpanCount();
                    }
                    return spanSizeBtPosition;
//                    if (getItemViewType(position) == TYPE_HEADER || getItemViewType(position) == TYPE_BOTTOM) {
//                    return gridManager.getSpanCount();
//                    }
//                    return 1;
                }
            });
        }
    }

    public int getSpanSizeBtPosition(int position) {
        if (getItemViewType(position) == TYPE_HEADER || getItemViewType(position) == TYPE_BOTTOM) {
            return -1;
        }
        return 1;
    }

    @Override
    public void onViewAttachedToWindow(BaseViewHolder holder) {
        ViewGroup.LayoutParams lp = holder.itemView.getLayoutParams();
        if (lp instanceof StaggeredGridLayoutManager.LayoutParams
                && holder.getLayoutPosition() == 0) {
            StaggeredGridLayoutManager.LayoutParams p = (StaggeredGridLayoutManager.LayoutParams) lp;
            p.setFullSpan(true);
        }
    }

    @Override
    public int getItemCount() {
        int count = dataList.size();
        if (headerView != null) {
            count++;
        }
        if (BottomView != null) {
            count++;
        }
        return count;
    }

    /**
     * 刷新头部HeadView数据
     * 一般不需要
     */
    public void notifyChangedHeadView() {
        if (headerView == null) {
            notifyItemChanged(dataList.size() + 1);
            return;
        }
        notifyItemChanged(dataList.size() + 2);
    }

    /**
     * 刷新底部部BottomView数据
     * 一般不需要
     */
    public void notifyChangedBottomView() {
        if (headerView == null) {
            return;
        }
        notifyItemChanged(0);
    }

    /**
     * 刷新数据指定位置数据
     *
     * @param position 数据列表位置
     */
    public void notifyChangedPosition(int position) {
        // 有headView 第一个为headView
        if (headerView != null) {
            notifyItemChanged(position + 1);
        } else {
            notifyItemChanged(position);
        }
    }


    /**
     * 设置单个item点击事件
     *
     * @param listener 点击事件
     */
    public void setOnItemClickListener(OnItemClickListener<T> listener) {
        itemListener = listener;
    }

    public void setItemViewClickListener(OnItemViewClickListener<T> listener) {
        itemViewClickListener = listener;
    }

    /**
     * 设置HeadView
     *
     * @param headerView 需要设置的HeadView
     */
    public void setHeaderView(View headerView) {
        if (headerView == null) {
            return;
        }
        headerView.setLayoutParams(new RecyclerView.LayoutParams(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT));
        this.headerView = headerView;
        notifyItemInserted(0);
    }

    /**
     * 获取HeadView
     *
     * @return 设置的HeadView
     * 可能为空
     */
    public View getHeaderView() {
        return headerView;
    }

    /**
     * 设置BottomView
     *
     * @param BottomView 需要设置的BottomView
     */
    public void setBottomView(View BottomView) {
        if (BottomView == null) {
            return;
        }
        BottomView.setLayoutParams(new RecyclerView.LayoutParams(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT));
        this.BottomView = BottomView;
        notifyItemInserted(0);
    }

    /**
     * 获取BottomView
     *
     * @return 设置的BottomView
     * 可能为空
     */
    public View getBottomView() {
        return BottomView;
    }

    /**
     * 添加数据列表
     *
     * @param data 数据列表
     */
    public void addData(List<T> data) {//刷新数据
        this.dataList.addAll(data);
        notifyDataSetChanged();
    }
    /**
     * 添加数据列表
     *
     * @param data 数据
     */
    public void addData(T data) {//刷新数据
        this.dataList.add(data);
        notifyItemChanged(dataList.size() - 1);
    }

    /**
     * 设置数据列表
     *
     * @param dataList 数据列表
     */
    public void setDataList(List<T> dataList) {
        if (dataList == null) {
            return;
        }
        this.dataList.clear();
        this.dataList.addAll(dataList);
    }

    public List<T> getDataList() {
        return dataList;
    }

    public void clearData() {
        this.dataList.clear();
        notifyDataSetChanged();
    }

    /**
     * 获取ViewHolder的在列表数据中的位置
     *
     * @param holder
     * @return
     */
    public int getRealPosition(RecyclerView.ViewHolder holder) {
        int position = holder.getLayoutPosition();
        return headerView == null ? position : position - 1;
    }

    /**
     * 创建itemView跟布局
     *
     * @param parent   父布局
     * @param viewType 布局类型
     * @return ViewHolder
     */
    public abstract BaseViewHolder onCreate(ViewGroup parent, final int viewType);

    /**
     * 绑定数据
     *
     * @param viewHolder   ViewHolder
     * @param RealPosition 列表中的位置
     * @param data         数据
     */
    public abstract void onBind(BaseViewHolder viewHolder, int RealPosition, T data);


    /**
     * 单个item点击事件
     *
     * @param <T> recyclerView中的数据泛型
     */
    public interface OnItemClickListener<T> {
        void onItemClick(int position, T data);
    }

    /**
     * 单个item点击事件
     *
     * @param <T> recyclerView中的数据泛型
     */
    public interface OnItemViewClickListener<T> {
        void onItemClick(int position, T data, View itemView);
    }

}
