package com.brsya.autonat.util;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Build;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.RecyclerView;


/**
 * FileName: UiUtil
 * Author: brsya
 * Date: 2020/11/24 17:18
 * Description: ui相关工具类
 */
public class UiUtil {

    /**
     * 获取状态栏高度
     */
    public static int getStatusBarHeight(Context context) {
        if (context == null) {
            return 0;
        }
        Resources resources = context.getResources();
        int resourceId = resources.getIdentifier("status_bar_height", "dimen", "android");
        //        int resourceId = resources.getIdentifier("status_bar_height", null, "android");
        return resources.getDimensionPixelSize(resourceId);
        //        return (int) resources.getDimension(resourceId);
    }

    /**
     * 获取状态栏高度
     */
    public static int getStatusBarHeightDp(Context context) {
        if (context == null) {
            return 0;
        }
        int width = context.getResources().getDisplayMetrics().widthPixels;
        Resources resources = context.getResources();
        int resourceId = resources.getIdentifier("status_bar_height", "dimen", "android");
        //        int resourceId = resources.getIdentifier("status_bar_height", null, "android");
        //        return resources.getDimensionPixelSize(resourceId);
        return (int) (resources.getDimension(resourceId) * 360d / width);
    }

    /**
     * 获取状态栏高度
     */
    public static int getStatusBarHeightPx(Context context) {
        return (int) (getStatusBarHeight(context) * context.getResources().getDisplayMetrics().density / Resources.getSystem().getDisplayMetrics().density);
    }

    /**
     * 设置上边距增加状态栏高度
     */
    public static void setStatusPadding(Context context, View view) {
        view.setPadding(
                view.getPaddingLeft(),
                view.getPaddingTop() + getStatusBarHeightPx(context),
                view.getPaddingRight(),
                view.getPaddingBottom()
        );
    }

    /**
     * dp转px
     *
     * @param dp dp值
     * @return 转换后的px
     */
    public static int dpToPx(Context context, float dp) {
        return (int) (context.getResources().getDisplayMetrics().density * dp);
    }

    /**
     * sp转px
     *
     * @param sp dp值
     * @return 转换后的px
     */
    public static int spToPx(Context context, float sp) {
        return (int) (TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, sp, context.getResources().getDisplayMetrics()));
    }


    /**
     * 设置状态栏颜色
     *
     * @param activity activity
     * @param isDark   true 透明黑字， false 透明白字
     */
    public static void setStatusDark(Activity activity, boolean isDark) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {//5.0及以上
            Window window = activity.getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS
                    | WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
            if (isDark) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN  //设置为全屏
                            | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);//状态栏字体颜色设置为黑色这个是Android 6.0才出现的属性   默认是白色
                } else {
                    window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN  //设置为全屏
                            | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);//状态栏字体颜色设置为黑色这个是Android 6.0才出现的属性   默认是白色
                }
            } else {
                window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);  //设置为全屏
            }
            //需要设置这个 flag 才能调用 setStatusBarColor 来设置状态栏颜色
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);//设置为透明色
            window.setNavigationBarColor(Color.TRANSPARENT);
        } else {//4.4到5.0
            WindowManager.LayoutParams localLayoutParams = activity.getWindow().getAttributes();
            localLayoutParams.flags = (WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS | localLayoutParams.flags);
        }
    }


    /**
     * 判断RecyclerView是否滑动到底部
     *
     * @param recyclerView 需要判断的RecyclerView
     * @return 是否滑动到底部
     */
    public static boolean isSlideToBottom(RecyclerView recyclerView) {
        return isSlideToBottomPx(recyclerView, 0);
    }

    /**
     * 判断RecyclerView是否滑动到距离底部像素值得位置
     *
     * @param recyclerView 需要判断的RecyclerView
     * @param px 距离底部像素值
     * @return 是否滑动到底部
     */
    public static boolean isSlideToBottomPx(RecyclerView recyclerView, int px) {
        if (recyclerView == null) return false;
        return recyclerView.computeVerticalScrollExtent() + recyclerView.computeVerticalScrollOffset() >= recyclerView.computeVerticalScrollRange() - px;
    }

    /**
     * 判断RecyclerView是否滑动到底部
     *
     * @param nestedScrollView 需要判断的RecyclerView
     * @return 是否滑动到底部
     */
    public static boolean isSlideToBottom(NestedScrollView nestedScrollView) {
        int scrollY = nestedScrollView.getScrollY();
        View onlyChild = nestedScrollView.getChildAt(0);
        if (onlyChild.getHeight() <= scrollY + nestedScrollView.getHeight()) {   // 如果满足就是到底部了
            return true;
        }
        return false;
    }

    /**
     * 控制状态栏是否显示
     *
     * @param activity     activity
     * @param isVisibility 是否显示
     */
    public static void statusVisibility(Activity activity, boolean isVisibility) {
        View decorView = activity.getWindow().getDecorView();
        if (isVisibility) {
            decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_VISIBLE | View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_IMMERSIVE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        } else {
            decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_IMMERSIVE);
        }
    }

    /**
     * 控制导航栏和状态栏是否显示
     *
     * @param activity     activity
     * @param isVisibility 是否显示
     */
    public static void navigationOrStatusVisibility(Activity activity, boolean isVisibility) {
        View decorView = activity.getWindow().getDecorView();
        if (isVisibility) {
            decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_VISIBLE | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_IMMERSIVE);
        } else {
            decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_IMMERSIVE);
        }
    }

    /**
     * 控制保持屏幕开启
     *
     * @param activity   activity
     * @param openScreen 是否保持屏幕开启
     */
    public static void openScreen(Activity activity, boolean openScreen) {
        if (openScreen) {
            activity.getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        } else {
            activity.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        }
    }

    public static int getScreenWidth(Context context) {
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        DisplayMetrics displayMetrics = new DisplayMetrics();
        wm.getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics.widthPixels;
    }

    public static int getScreenHeight(Context context) {
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        DisplayMetrics displayMetrics = new DisplayMetrics();
        wm.getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics.heightPixels;
    }

    /**
     * 根据手机的分辨率从 px(像素) 的单位 转成为 dp(相对大小)
     */
    public static int pxToDp(Context context, float pxValue) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (pxValue / scale + 0.5f);
    }
}
